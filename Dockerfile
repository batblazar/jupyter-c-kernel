FROM jupyter/minimal-notebook
MAINTAINER Brendan Rius <ping@brendan-rius.com>

USER root

WORKDIR /tmp

#COPY ./ jupyter_c_kernel/

#RUN pip install --no-cache-dir jupyter_c_kernel/
RUN pip install pyarrow pandas psycopg2-binary googletrans==4.0.0-rc1
#RUN cd jupyter_c_kernel && install_c_kernel --user
RUN apt-get update && apt-get install gcc iputils-ping postgresql-client -y
WORKDIR /home/$NB_USER/

USER $NB_USER
